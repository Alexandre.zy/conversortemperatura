/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import conversortemperatura.ConversorTemperatura;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author alexandre
 */
public class conversorTest {
    
    public class testConversor {

private ConversorTemperatura meuConversor;
    
    @Before
    
    public void setUp() throws Exception {
        meuConversor = new ConversorTemperatura();
    }
    
    @Test
    public void testCelsiusParaFahrenheitComValorDouble() {
         assertEquals(104.0, meuConversor.celsiusParaFahrenheit(40.0),0.01);
    }
    
    @Test
    public void testcelsiusParaKelvinComValorDouble() {
         assertEquals(313.0, meuConversor.celsiusParaKelvin(40.0),0.01);
    }
    
    @Test
    public void testfahrenheitParaCelsiusComValorDouble() {
         assertEquals(10.0, meuConversor.fahrenheitParaCelsius(50.0),0.01);
    }
    
    @Test
    public void testfahrenheitParaKelvinComValorDouble() {
         assertEquals(277.4444444444444, meuConversor.fahrenheitParaKelvin(40.0),0.01);
    }
    
    @Test
    public void testkelvinParaCelsiusComValorDouble() {
         assertEquals(-233.0, meuConversor.kelvinParaCelsius(40.0),0.01);
    }
    
    @Test
    public void testkelvinParaFahrenheitComValorDouble() {
         assertEquals(-387.40000000000001, meuConversor.kelvinParaFahrenheit(40.0),0.01);
    }
    
    
    }
}
